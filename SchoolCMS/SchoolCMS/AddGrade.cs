﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace sqlite_crud
{
    public partial class AddGrade : Form
    {
        public AddGrade()
        {
            InitializeComponent();
        }

        GradeList g = null;
        string tid = null;

        private void button2_Click(object sender, EventArgs e)
        {
            string sql = string.Format(@"insert into grade (gname, tid) values('{0}', {1})", textBox1.Text, tid);
            int i = DBhelp.ExecuteNonQuery(sql);
            if (i > 0)
            {
                if (g == null)
                {
                    g = new GradeList();
                    g.Show();
                }
                else
                {
                    g.Reload();
                }
                DialogResult dialogResult = MessageBox.Show("新增成功，是否需要继续新增", "新增提示", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (dialogResult == DialogResult.Yes)
                {
                    this.TopMost = true;
                }
                else if (dialogResult == DialogResult.No)
                {
                    this.Hide();
                }
            }
            else
            {
                MessageBox.Show("新增失败");
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //获取tid
            string sql1 = string.Format(@"SELECT * FROM teacher where tname = '{0}'", comboBox1.Text);
            MySqlDataReader res1 = DBhelp.executeQuery(sql1);
            while (res1.Read())
            {
                tid = res1["tid"].ToString();
                string tage = res1["tage"].ToString();
                this.textBox2.Text = tage;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            comboBox1.SelectedIndex = 0;
        }

        private void AddGrade_Load(object sender, EventArgs e)
        {
            string sql = string.Format(@"SELECT * FROM teacher");
            MySqlDataReader res = DBhelp.executeQuery(sql);
            while (res.Read())
            {
                string tname = res["tname"].ToString();
                this.comboBox1.Items.Add(tname);
            }
            // 设置下拉框初始值
            comboBox1.Items.Insert(0, "请选择");
            this.comboBox1.SelectedIndex = 0;
        }
    }
}
