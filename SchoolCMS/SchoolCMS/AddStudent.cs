﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace sqlite_crud
{
    public partial class AddStudent : Form
    {
        public AddStudent()
        {
            InitializeComponent();
        }

        string gid = null; //班级号
        StuList s = null; //已经打开就不再new了

        private void AddStudent_Load(object sender, EventArgs e)
        {
            string sql = string.Format(@"SELECT * FROM grade");
            MySqlDataReader res = DBhelp.executeQuery(sql);
            while (res.Read())
            {
                string gname = res["gname"].ToString();
                this.comboBox1.Items.Add(gname);
            }
            // 设置下拉框初始值
            comboBox1.Items.Insert(0, "请选择");
            this.comboBox1.SelectedIndex = 0;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string sql = string.Format(@"insert into student (sname, sage, gid, username) values('{0}', {1}, {2}, '{3}')", textBox1.Text, textBox2.Text, gid, textBox4.Text);
            int i = DBhelp.ExecuteNonQuery(sql);
            if (i > 0)
            {
                if (s == null)
                {
                    s = new StuList();
                    s.Show();
                }
                else
                {
                    s.Reload();
                }
                DialogResult dialogResult = MessageBox.Show("新增成功，是否需要继续新增", "新增提示", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (dialogResult == DialogResult.Yes)
                {
                    this.TopMost = true;
                }
                else if (dialogResult == DialogResult.No)
                {
                    this.Hide();
                }
            } else
            {
                MessageBox.Show("新增失败");
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //获取gid
            string sql1 = string.Format(@"SELECT * FROM grade where gname = '{0}'", comboBox1.Text);
            MySqlDataReader res1 = DBhelp.executeQuery(sql1);
            while (res1.Read())
            {
                gid = res1["gid"].ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox4.Text = "";
            comboBox1.SelectedIndex = 0;
        }
    }
}
