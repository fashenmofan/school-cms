﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sqlite_crud
{
    public partial class AddTeacher : Form
    {
        public AddTeacher()
        {
            InitializeComponent();
        }

        TeaList t = null;

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox4.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string sql = string.Format(@"insert into teacher (tname, tage, username) values('{0}', {1}, '{2}')", textBox1.Text, textBox2.Text, textBox4.Text);
            int i = DBhelp.ExecuteNonQuery(sql);
            if (i > 0)
            {
                if (t == null)
                {
                    t = new TeaList(null);
                    t.Show();
                }
                else
                {
                    t.Reload();
                }
                DialogResult dialogResult = MessageBox.Show("新增成功，是否需要继续新增", "新增提示", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (dialogResult == DialogResult.Yes)
                {
                    this.TopMost = true;
                }
                else if (dialogResult == DialogResult.No)
                {
                    this.Hide();
                }
            }
            else
            {
                MessageBox.Show("新增失败");
            }
        }
    }
}
