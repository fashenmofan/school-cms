﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace sqlite_crud
{
    internal class DBhelp
    {
        private static String connStr = "server=localhost;port=3306;database=csharp;uid=root;pwd=1717;charset=utf8;sslmode=none";
        
        //声明链接
        public static MySqlConnection GetConn()
        {
            MySqlConnection conn = new MySqlConnection(connStr);//
            if (conn.State == ConnectionState.Closed) conn.Open();//判断是否关闭 打开链接
            return conn;//返回链接
        }
        public static void CloseAll(MySqlConnection conn)
        {
            if (conn == null) return;
            if (conn.State == ConnectionState.Open || conn.State == ConnectionState.Connecting)
                //判断
                conn.Close();//关闭链接
        }
        public static int ExecuteNonQuery(string sql) // 增删改 封装 返回受影响的行
        {
            MySqlConnection conn = null;
            int result; //提前设置一个返回值
            try
            {
                conn = GetConn();//获取链接
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                result = cmd.ExecuteNonQuery();//执行sql 并返回受影响的行的个数
            }
            finally
            {
                CloseAll(conn);
            }
            return result;
        }
        public static MySqlDataReader executeQuery(string sql) //查询
        {
            MySqlDataReader res = null;
            MySqlConnection conn = null;
            try
            {
                MySqlConnection conn1 = GetConn();
                MySqlCommand cmd = new MySqlCommand(sql, conn1);
                res = cmd.ExecuteReader();
            }
            finally
            {
                CloseAll(conn);
            }
            return res;
        }
    }

}
