﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace sqlite_crud
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        /**
         * 数据库连接基础练习
         */
        private void button1_Click(object sender, EventArgs e)
        {
            // 根据组件的属性name获取 输入框(TextBox)里面的数据
            // this：当前窗口使用 name：属性名 Text：文本
            string sname = this.sname.Text;
            string sage = this.sage.Text;

            // 连接数据库的操作 
            // 1: 声明一个数据源 目的：找到我的收据库
            /* 
             * server:服务 localhost/127.0.0.1（本地） 
             * port：端口号 MySQL数据库的端口号就是3306 所以契合 
             * database：数据库 声明我要用哪个数据库 
             * uid：数据库用户名 
             * pwd:数据库的密码 
             */ 
            string url = "server=127.0.0.1; port=3306;database=csharp;uid=root;pwd=1717"; 
            // 2：创建 链接 
            // 注意 : 我们链接数据库的时候需要引用 mysql.data.dll
            MySqlConnection con = new MySqlConnection(url); 
            // 3: 打开数据库
            con.Open();
            MessageBox.Show("打开数据库");
            //4： 声明一个sql 
            // 既然 数据库已经打开了 我是不是可以对数据库进行一些操作
            // 注意：SQL字符串需要用引号括着
            string sql = string.Format("INSERT INTO student (sname,sage) VALUES('{0}', {1})", sname, sage);
            // 5：创建一个操作 sql的对象（MySqlCommand）
            MySqlCommand com = new MySqlCommand(sql, con); 
            // 6: 执行结束之后 返回一个结果 整数 int 
            // ExecuteNonQuery: 执行增删改的 新增 修改 删除
            int i= com.ExecuteNonQuery(); 
            if (i > 0) {
                //类似于js中的console.log()提示框
                MessageBox.Show("新增成功");
            } else {
                MessageBox.Show("失败"); 
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
