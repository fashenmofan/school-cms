﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace sqlite_crud
{
    public partial class GradeList : Form
    {
        public GradeList()
        {
            InitializeComponent();
        }

        string gid = null;
        string tid = null;

        public void Reload()
        {
            this.GradeList_Load(null, null);
        }

        private void GradeList_Load(object sender, EventArgs e)
        {
            // 修改成功后，需要加载，查询页面，此时有重复数据，所以要清除一下。
            this.listView1.Items.Clear();
            // 窗体加载事件——Load事件
            string sql = string.Format(@"SELECT * FROM grade a LEFT JOIN teacher b ON a.tid = b.tid");
            MySqlDataReader res = DBhelp.executeQuery(sql);
            while (res.Read())
            {
                string gid = res["gid"].ToString();
                string gname = res["gname"].ToString();
                string tname = res["tname"].ToString();
                string tage = res["tage"].ToString();

                // 将数据加载到ListView控件中
                ListViewItem list = new ListViewItem();
                // id 常规来说不需要展示 给用户
                list.Tag = gid;
                list.Text = gname;
                // 添加子菜单
                list.SubItems.Add(tname);
                list.SubItems.Add(tage);
                // 向列表中添加行
                this.listView1.Items.Add(list);
            }

            sql = string.Format(@"SELECT * FROM teacher");
            res = DBhelp.executeQuery(sql);
            while (res.Read())
            {
                string tname = res["tname"].ToString();
                this.comboBox1.Items.Add(tname);
            }
            // 设置下拉框初始值
            comboBox1.Items.Insert(0, "请选择");
            this.comboBox1.SelectedIndex = 0;

            if (this.listView1.Items.Count > 0)
            {
                this.listView1.Items[0].Selected = true;
                this.listView1.Select();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string sql = string.Format(@"UPDATE grade SET gname='{0}', tid={1} WHERE gid={2}", textBox1.Text, tid, gid);
            int i = DBhelp.ExecuteNonQuery(sql);
            if (i > 0)
            {
                MessageBox.Show("修改成功");
                GradeList_Load(sender, e);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //获取gid
            string sql1 = string.Format(@"SELECT * FROM teacher where tname = '{0}'", comboBox1.Text);
            MySqlDataReader res1 = DBhelp.executeQuery(sql1);
            while (res1.Read())
            {
                tid = res1["tid"].ToString();
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //判断有没有选中数据，选中后回显数据
            if (this.listView1.SelectedItems.Count > 0)
            {
                // 拿到选中数据 索引值从0开始:因为每次选中一行，所有索引值为0
                ListViewItem lis = this.listView1.SelectedItems[0];
                gid = lis.Tag.ToString();
                string gname = lis.Text;
                // 获取子菜单
                string tname = lis.SubItems[1].Text;
                //把拿到的数据赋值在修改框中
                textBox1.Text = gname;
                int index = this.comboBox1.FindString(tname);
                this.comboBox1.SelectedIndex = index;
            }
        }
    }
}
