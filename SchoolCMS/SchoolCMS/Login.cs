﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace sqlite_crud
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string password = null;
            // 判断是不是学生登录
            if (radioButton1.Checked == true)
            {
                // string.Format格式化字符串
                string sql = string.Format(@"SELECT password FROM student WHERE username='{0}'", textBox1.Text);
                // 执行sql    返回一个结果集
                MySqlDataReader res = DBhelp.executeQuery(sql);
                int count = 0;
                // 一行一行 往下读
                while (res.Read())
                {
                    password = res["password"].ToString();
                    count++;
                }
                string username = this.textBox1.Text;
                if (count > 0 && password.Equals(textBox2.Text))
                {
                    MessageBox.Show("登录成功");
                    // 创建 学生菜单的实例
                    StudentMenu stu = new StudentMenu(this, username);
                    // 这个是展示窗口
                    stu.Show();
                    // 展示窗体之后，把前一个打开的窗体关闭
                    this.Hide();
                } else
                {
                    MessageBox.Show("账号密码错误");
                }
            } else
            {
                string sql = string.Format(@"SELECT password FROM teacher WHERE username='{0}'", textBox1.Text);
                MySqlDataReader res = DBhelp.executeQuery(sql);
                while (res.Read())
                {
                    password = res["password"].ToString();
                }
                string username = this.textBox1.Text;
                if (password.Equals(textBox2.Text))
                {
                    MessageBox.Show("登录成功");
                    TeacherMenu teacher = new TeacherMenu(this, username);
                    teacher.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("账号密码失败");
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.reset();
        }

        public void reset()
        {
            // 重置按钮：清空输入框里面的值
            this.textBox1.Text = "";
            this.textBox2.Text = "";
            this.radioButton1.Checked = true;
            this.radioButton2.Checked = false;
        }
    }
}
