﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace sqlite_crud
{
    public partial class StuAccount1 : Form
    {
        public string prop = null;
        StudentMenu studentMenu = null;
        public StuAccount1(StudentMenu studentMenu)
        {
            InitializeComponent();
            this.studentMenu = studentMenu;
        }

        private void StuAccount1_Load(object sender, EventArgs e)
        {
            this.textBox1.AppendText(prop);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string sql = string.Format(@"UPDATE student SET username='{0}' WHERE username='{1}' and password='{2}'", textBox1.Text, prop, textBox2.Text);
            int i = DBhelp.ExecuteNonQuery(sql);
            if (i > 0)
            {
                MessageBox.Show("修改成功");
                this.studentMenu.changeUsername(textBox1.Text);
            }
            else
            {
                MessageBox.Show("修改失败");
            }
        }
    }
}
