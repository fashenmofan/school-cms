﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace sqlite_crud
{
    public partial class StuInfo : Form
    {
        //声明一个 变量接受数据
        public string prop;
        public StuInfo()
        {
            InitializeComponent();
        }

        string sid = null;

        private void StuInfo_Load(object sender, EventArgs e)
        {
            string sql = string.Format(@"SELECT * FROM student a LEFT JOIN grade b ON a.gid = b.gid where username='{0}'", prop);
            MySqlDataReader res = DBhelp.executeQuery(sql);
            string tid = null;
            while (res.Read())
            {
                sid = res["sid"].ToString();
                string sname = res["sname"].ToString();
                string sage = res["sage"].ToString();
                string username = res["username"].ToString();
                string gname = res["gname"].ToString();
                tid = res["tid"].ToString();

                this.textBox1.AppendText(sname);
                textBox2.Text = sage;
                textBox3.Text = gname;
            }

            sql = string.Format(@"SELECT * FROM teacher WHERE tid = {0}", tid);
            res = DBhelp.executeQuery(sql);
            while (res.Read())
            {
                string tname = res["tname"].ToString();

                textBox4.Text = tname;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string sql = string.Format(@"UPDATE student SET sname='{0}', sage={1} WHERE sid={2}", textBox1.Text, textBox2.Text, sid);
            int i = DBhelp.ExecuteNonQuery(sql);
            if (i > 0)
            {
                MessageBox.Show("修改成功");
                StuInfo_Load(sender, e);
            }
        }
    }
}
