﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace sqlite_crud
{
    public partial class StuList : Form
    {
        public StuList()
        {
            InitializeComponent();
        }

        string sid = null;
        string gid = null;

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //判断有没有选中数据，选中后回显数据
            if (this.listView1.SelectedItems.Count > 0)
            {
                // 拿到选中数据 索引值从0开始:因为每次选中一行，所有索引值为0
                ListViewItem lis = this.listView1.SelectedItems[0];
                sid = lis.Tag.ToString();
                string sname = lis.Text;
                // 获取子菜单
                string sage = lis.SubItems[1].Text;
                string gname = lis.SubItems[2].Text;
                string username = lis.SubItems[3].Text;
                //把拿到的数据赋值在修改框中
                textBox1.Text = sname;
                textBox2.Text = sage;
                textBox4.Text = username;
                int index = this.comboBox1.FindString(gname);
                this.comboBox1.SelectedIndex = index;
            }
        }

        public void Reload()
        {
            this.StuList_Load(null, null);
        }

        private void StuList_Load(object sender, EventArgs e)
        {
            // 修改成功后，需要加载，查询页面，此时有重复数据，所以要清除一下。
            this.listView1.Items.Clear();
            // 窗体加载事件——Load事件
            string sql = string.Format(@"SELECT * FROM student a LEFT JOIN grade b ON a.gid = b.gid");
            MySqlDataReader res = DBhelp.executeQuery(sql);
            while (res.Read())
            {
                string sid = res["sid"].ToString();
                string sname = res["sname"].ToString();
                string sage = res["sage"].ToString();
                string username = res["username"].ToString();
                string gname = res["gname"].ToString();

                // 将数据加载到ListView控件中
                ListViewItem list = new ListViewItem();
                // id 常规来说不需要展示 给用户
                list.Tag = sid;
                list.Text = sname;
                // 添加子菜单
                list.SubItems.Add(sage);
                list.SubItems.Add(gname);
                list.SubItems.Add(username);
                // 向列表中添加行
                this.listView1.Items.Add(list);
            }

            sql = string.Format(@"SELECT * FROM grade");
            res = DBhelp.executeQuery(sql);
            while (res.Read())
            {
                string gname = res["gname"].ToString();
                this.comboBox1.Items.Add(gname);
            }
            // 设置下拉框初始值
            comboBox1.Items.Insert(0, "请选择");
            this.comboBox1.SelectedIndex = 0;

            if (this.listView1.Items.Count > 0)
            {
                this.listView1.Items[0].Selected = true;
                this.listView1.Select();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //获取gid
            string sql1 = string.Format(@"SELECT * FROM grade where gname = '{0}'", comboBox1.Text);
            MySqlDataReader res1 = DBhelp.executeQuery(sql1);
            while (res1.Read())
            {
                gid = res1["gid"].ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string sql = string.Format(@"UPDATE student SET sname='{0}', sage={1}, gid={2}, username='{3}' WHERE sid={4}", textBox1.Text, textBox2.Text, gid, textBox4.Text, sid);
            int i = DBhelp.ExecuteNonQuery(sql);
            if (i > 0)
            {
                MessageBox.Show("修改成功");
                StuList_Load(sender, e);
            }
        }
    }
}
