﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sqlite_crud
{
    public partial class StudentMenu : Form
    {
        Login login = null;
        public StudentMenu(Login login, string username)
        {
            InitializeComponent();
            this.login = login;
            ppToolStripMenuItem.Text = username;
        }

        private void 班级管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void 学生管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StuInfo s = new StuInfo();
            s.prop = ppToolStripMenuItem.Text;
            s.Show();
        }

        private void StudentMenu_Load(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void 修改密码ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void 修改密码ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            StuAccount stuAccount = new StuAccount();
            stuAccount.prop = ppToolStripMenuItem.Text;
            stuAccount.Show();
        }

        private void 修改用户名ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StuAccount1 stuAccount1 = new StuAccount1(this);
            stuAccount1.prop = ppToolStripMenuItem.Text;
            stuAccount1.Show();
        }

        public void changeUsername(string username)
        {
            ppToolStripMenuItem.Text = username;
        }

        private void 登出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            // 遍历并关闭所有子窗口
            FormCollection childCollection = Application.OpenForms;
            for (int i = childCollection.Count; i-- > 0;)
            {
                if (childCollection[i].Name != "Login") childCollection[i].Close();
            }
            this.login.Show();
            this.login.reset();
        }
    }
}
