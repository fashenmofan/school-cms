﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace sqlite_crud
{
    public partial class TeaList : Form
    {
        TeacherMenu teacherMenu = null;
        public TeaList(TeacherMenu teacherMenu)
        {
            InitializeComponent();
            this.teacherMenu = teacherMenu;
        }

        string tid = null;

        public void Reload()
        {
            this.TeaList_Load(null, null);
        }

        private void TeaList_Load(object sender, EventArgs e)
        {
            // 修改成功后，需要加载，查询页面，此时有重复数据，所以要清除一下。
            this.listView1.Items.Clear();
            // 窗体加载事件——Load事件
            string sql = string.Format(@"SELECT * FROM teacher");
            MySqlDataReader res = DBhelp.executeQuery(sql);
            while (res.Read())
            {
                string tid = res["tid"].ToString();
                string tname = res["tname"].ToString();
                string tage = res["tage"].ToString();
                string username = res["username"].ToString();

                // 将数据加载到ListView控件中
                ListViewItem list = new ListViewItem();
                // id 常规来说不需要展示 给用户
                list.Tag = tid;
                list.Text = tname;
                // 添加子菜单
                list.SubItems.Add(tage);
                list.SubItems.Add(username);
                // 向列表中添加行
                this.listView1.Items.Add(list);
            }

            if (this.listView1.Items.Count > 0)
            {
                this.listView1.Items[0].Selected = true;
                this.listView1.Select();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string sql = string.Format(@"UPDATE teacher SET tname='{0}', tage={1}, username='{2}' WHERE tid={3}", textBox1.Text, textBox2.Text, textBox4.Text, tid);
            int i = DBhelp.ExecuteNonQuery(sql);
            if (i > 0)
            {
                MessageBox.Show("修改成功");
                TeaList_Load(sender, e);
                if (this.teacherMenu.tid == tid)
                {
                    this.teacherMenu.changeUsername(textBox4.Text);
                }
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //判断有没有选中数据，选中后回显数据
            if (this.listView1.SelectedItems.Count > 0)
            {
                // 拿到选中数据 索引值从0开始:因为每次选中一行，所有索引值为0
                ListViewItem lis = this.listView1.SelectedItems[0];
                tid = lis.Tag.ToString();
                string tname = lis.Text;
                // 获取子菜单
                string tage = lis.SubItems[1].Text;
                string username = lis.SubItems[2].Text;
                //把拿到的数据赋值在修改框中
                textBox1.Text = tname;
                textBox2.Text = tage;
                textBox4.Text = username;
            }
        }
    }
}
