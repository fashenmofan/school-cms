﻿namespace sqlite_crud
{
    partial class TeacherMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.学生管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.信息查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.新增数据ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.班级管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.信息查询ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.新增班级ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.老师管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.信息管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.新增老师ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.欢迎你ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qqToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.学生管理ToolStripMenuItem,
            this.班级管理ToolStripMenuItem,
            this.老师管理ToolStripMenuItem,
            this.退出ToolStripMenuItem,
            this.qqToolStripMenuItem,
            this.欢迎你ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 32);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 学生管理ToolStripMenuItem
            // 
            this.学生管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.信息查询ToolStripMenuItem,
            this.新增数据ToolStripMenuItem});
            this.学生管理ToolStripMenuItem.Name = "学生管理ToolStripMenuItem";
            this.学生管理ToolStripMenuItem.Size = new System.Drawing.Size(98, 28);
            this.学生管理ToolStripMenuItem.Text = "学生管理";
            // 
            // 信息查询ToolStripMenuItem
            // 
            this.信息查询ToolStripMenuItem.Name = "信息查询ToolStripMenuItem";
            this.信息查询ToolStripMenuItem.Size = new System.Drawing.Size(270, 34);
            this.信息查询ToolStripMenuItem.Text = "信息查询";
            this.信息查询ToolStripMenuItem.Click += new System.EventHandler(this.信息查询ToolStripMenuItem_Click);
            // 
            // 新增数据ToolStripMenuItem
            // 
            this.新增数据ToolStripMenuItem.Name = "新增数据ToolStripMenuItem";
            this.新增数据ToolStripMenuItem.Size = new System.Drawing.Size(270, 34);
            this.新增数据ToolStripMenuItem.Text = "新增学生";
            this.新增数据ToolStripMenuItem.Click += new System.EventHandler(this.新增数据ToolStripMenuItem_Click);
            // 
            // 班级管理ToolStripMenuItem
            // 
            this.班级管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.信息查询ToolStripMenuItem1,
            this.新增班级ToolStripMenuItem});
            this.班级管理ToolStripMenuItem.Name = "班级管理ToolStripMenuItem";
            this.班级管理ToolStripMenuItem.Size = new System.Drawing.Size(98, 28);
            this.班级管理ToolStripMenuItem.Text = "班级管理";
            // 
            // 信息查询ToolStripMenuItem1
            // 
            this.信息查询ToolStripMenuItem1.Name = "信息查询ToolStripMenuItem1";
            this.信息查询ToolStripMenuItem1.Size = new System.Drawing.Size(182, 34);
            this.信息查询ToolStripMenuItem1.Text = "信息查询";
            this.信息查询ToolStripMenuItem1.Click += new System.EventHandler(this.信息查询ToolStripMenuItem1_Click);
            // 
            // 新增班级ToolStripMenuItem
            // 
            this.新增班级ToolStripMenuItem.Name = "新增班级ToolStripMenuItem";
            this.新增班级ToolStripMenuItem.Size = new System.Drawing.Size(182, 34);
            this.新增班级ToolStripMenuItem.Text = "新增班级";
            this.新增班级ToolStripMenuItem.Click += new System.EventHandler(this.新增班级ToolStripMenuItem_Click);
            // 
            // 老师管理ToolStripMenuItem
            // 
            this.老师管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.信息管理ToolStripMenuItem,
            this.新增老师ToolStripMenuItem});
            this.老师管理ToolStripMenuItem.Name = "老师管理ToolStripMenuItem";
            this.老师管理ToolStripMenuItem.Size = new System.Drawing.Size(98, 28);
            this.老师管理ToolStripMenuItem.Text = "老师管理";
            // 
            // 信息管理ToolStripMenuItem
            // 
            this.信息管理ToolStripMenuItem.Name = "信息管理ToolStripMenuItem";
            this.信息管理ToolStripMenuItem.Size = new System.Drawing.Size(270, 34);
            this.信息管理ToolStripMenuItem.Text = "信息管理";
            this.信息管理ToolStripMenuItem.Click += new System.EventHandler(this.信息管理ToolStripMenuItem_Click);
            // 
            // 新增老师ToolStripMenuItem
            // 
            this.新增老师ToolStripMenuItem.Name = "新增老师ToolStripMenuItem";
            this.新增老师ToolStripMenuItem.Size = new System.Drawing.Size(270, 34);
            this.新增老师ToolStripMenuItem.Text = "新增老师";
            this.新增老师ToolStripMenuItem.Click += new System.EventHandler(this.新增老师ToolStripMenuItem_Click);
            // 
            // 退出ToolStripMenuItem
            // 
            this.退出ToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            this.退出ToolStripMenuItem.Size = new System.Drawing.Size(62, 28);
            this.退出ToolStripMenuItem.Text = "登出";
            this.退出ToolStripMenuItem.Click += new System.EventHandler(this.退出ToolStripMenuItem_Click);
            // 
            // 欢迎你ToolStripMenuItem
            // 
            this.欢迎你ToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.欢迎你ToolStripMenuItem.Name = "欢迎你ToolStripMenuItem";
            this.欢迎你ToolStripMenuItem.Size = new System.Drawing.Size(98, 28);
            this.欢迎你ToolStripMenuItem.Text = "欢迎你：";
            // 
            // qqToolStripMenuItem
            // 
            this.qqToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.qqToolStripMenuItem.Name = "qqToolStripMenuItem";
            this.qqToolStripMenuItem.Size = new System.Drawing.Size(50, 28);
            this.qqToolStripMenuItem.Text = "qq";
            this.qqToolStripMenuItem.Click += new System.EventHandler(this.qqToolStripMenuItem_Click);
            // 
            // TeacherMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "TeacherMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TeacherMenu";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.TeacherMenu_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 学生管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 班级管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 信息查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 新增数据ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 信息查询ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 新增班级ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 老师管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 信息管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 新增老师ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem qqToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 欢迎你ToolStripMenuItem;
    }
}