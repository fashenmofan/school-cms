﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace sqlite_crud
{
    public partial class TeacherMenu : Form
    {
        Login login = null;
        public string tid;
        public TeacherMenu(Login login, string username)
        {
            InitializeComponent();
            this.login = login;
            qqToolStripMenuItem.Text = username;
            tid = this.getTid(username);
            qqToolStripMenuItem.Tag = tid;
        }

        public void changeUsername(string username)
        {
            qqToolStripMenuItem.Text = username;
        }

        public string getTid(string username)
        {
            string tid = null;
            string sql = string.Format(@"SELECT tid FROM teacher WHERE username='{0}'", username);
            MySqlDataReader res = DBhelp.executeQuery(sql);
            // 一行一行 往下读
            while (res.Read())
            {
                tid = res["tid"].ToString();
            }
            return tid;
        }

        private void 新增数据ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddStudent addStu = new AddStudent();
            addStu.Show();
        }

        private void 信息查询ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StuList stu = new StuList();
            stu.Show();
        }

        private void 信息查询ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            GradeList gradeList = new GradeList();
            gradeList.Show();
        }

        private void 信息管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TeaList teaList = new TeaList(this);
            teaList.Show();
        }

        private void 新增老师ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddTeacher addTeacher = new AddTeacher();
            addTeacher.Show();
        }

        private void 新增班级ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddGrade addGrade = new AddGrade();
            addGrade.Show();
        }

        private void 退出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            // 遍历并关闭所有子窗口
            FormCollection childCollection = Application.OpenForms;
            for (int i = childCollection.Count; i-- > 0;)
            {
                if (childCollection[i].Name != "Login") childCollection[i].Close();
            }
            this.login.Show();
            this.login.reset();
        }

        private void qqToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void TeacherMenu_Load(object sender, EventArgs e)
        {

        }
    }
}
