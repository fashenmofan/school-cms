/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.15 : Database - csharp
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`csharp` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `csharp`;

/*Table structure for table `grade` */

DROP TABLE IF EXISTS `grade`;

CREATE TABLE `grade` (
  `gid` int(11) NOT NULL AUTO_INCREMENT,
  `gname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tid` int(11) DEFAULT NULL,
  PRIMARY KEY (`gid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `grade` */

insert  into `grade`(`gid`,`gname`,`tid`) values (1,'2020',1),(2,'2018',4),(3,'1999',3),(4,'测试二',2);

/*Table structure for table `student` */

DROP TABLE IF EXISTS `student`;

CREATE TABLE `student` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `sname` varchar(255) DEFAULT NULL,
  `sage` int(11) DEFAULT NULL,
  `gid` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT 'admin',
  `password` varchar(255) DEFAULT '000000',
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

/*Data for the table `student` */

insert  into `student`(`sid`,`sname`,`sage`,`gid`,`username`,`password`) values (1,'张三',18,1,'admin','000000'),(2,'张无忌',22,2,'sadf',NULL),(5,'赵敏',55,1,'asdf',NULL),(6,'周芷若',19,2,'gfdfg',NULL),(7,'周日',18,1,'sdf',NULL),(8,'周二',19,2,'sfagdf',NULL),(9,'周三',20,1,'asgfd',NULL),(10,'赵敏',55,1,'sfggdf',NULL),(11,'大家好',12,1,'dfgdgf',NULL),(12,'李昱威',18,2,'dfgfg',NULL),(13,'赵四',2000,2,'dfgg',NULL),(14,'赵高',12,2,'dghg',NULL),(15,'拉拉',18,1,'渣渣','000000'),(30,'嘎嘎',18,2,'拉拉','000000'),(31,'你好',1,1,'Thank you','000000'),(32,'大家好',2,2,'dajiahao','000000'),(33,'hello',12,1,'年后','000000'),(34,'hello',12,1,'年后','000000'),(35,'csharp',19,1,'大麻','000000'),(36,'大迈',12,2,'username','000000'),(37,'大地',12,2,'username','000000'),(38,'打野',18,1,'大叶','000000'),(39,'你好',19,2,'你好','000000');

/*Table structure for table `t_student` */

DROP TABLE IF EXISTS `t_student`;

CREATE TABLE `t_student` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `age` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `t_student` */

insert  into `t_student`(`id`,`name`,`age`) values (1,'zhangsan',25),(2,'lisi',28),(3,'wangwu',23),(4,'Tom',21),(5,'Jck',55),(6,'Lucy',27),(7,'zhaoliu',75);

/*Table structure for table `teacher` */

DROP TABLE IF EXISTS `teacher`;

CREATE TABLE `teacher` (
  `tid` int(11) NOT NULL AUTO_INCREMENT,
  `tname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tage` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'admin',
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT '000000',
  PRIMARY KEY (`tid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `teacher` */

insert  into `teacher`(`tid`,`tname`,`tage`,`username`,`password`) values (1,'蔡美鸡',33,'admin','000000'),(2,'鸡你太美',44,'小老师','000000'),(3,'你好,老师',22,'大老师','000000'),(4,'测试1',18,'test','000000');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(256) DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
